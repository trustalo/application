# Trustalo

Trustalo is a management system for traking and coordinating work done in [Trashware](https://simple.wikipedia.org/wiki/Trashware) repair workshops.

This project is under development and tailored to [Trashware Cesena](http://trashwarecesena.it/) needs, but aims to become an usefull tool for all the traswhares out there!
